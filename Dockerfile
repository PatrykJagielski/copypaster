FROM nginx:1.19-alpine

EXPOSE 8081

COPY ./frontend/paste.html /usr/share/nginx/html/index.html
COPY ./frontend/media/favicon_P.ico /usr/share/nginx/html/favicon.ico
COPY ./frontend/media/Letter-P-icon.png /usr/share/nginx/html/Letter-P-icon.png

CMD ["nginx", "-g", "daemon off;"]
