from flask import Flask
from flask_cors import CORS
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

parser = reqparse.RequestParser()
parser.add_argument('clipboard')

Clipboard = ""


class Copypaster(Resource):
    def get(self):
        return Clipboard
    
    def post(self):
        args = parser.parse_args()
        if args:
            global Clipboard
            Clipboard = args.get('clipboard')
        return "", 201


api.add_resource(Copypaster, '/')

if __name__ == '__main__':
    from waitress import serve
    serve(app, host="0.0.0.0", port=5001)
